"use strict";

// Librerias
var validateFormLogin     = require('./libs/validateFormLogin.js'),
    validateFormRegister  = require('./libs/validateFormRegister.js'),
    Autofocus             = require('./libs/autoFocus.js'),
    Ubigeo                = require('./libs/ubigeo.js'),
    Navegador             = require('./libs/browser.js'),
    twDialog              = require('./libs/twShare.js'),
    fbDialog              = require('./libs/fbShare.js');

// Componentes
//var Posts = require('./components/post.js');


var APP, UTIL;

APP = {

    /* Funciones y métodos que se aplican al site en general */
    common: {
        init: function() {

            var browser = new Navegador();

            if ( browser.isVersion('tablet') ) {
                console.log("Es Android Tablet");
            }
        }
    },


    /* Home */
    home: {
        init: function() {
        }
    },


    /* Login */
    login: {
        init: function() {

            /* Invocar Autofocus */
            var autoFocus = new Autofocus(
                'day_born', 'month_born', 'year_born', 'dni'
            );

            /* Autocompleta ceros en campos del login */
            $('#day_born, #month_born').focusout(function() {

                var field = $(this),
                    value = parseInt( field.val() );

                if ( value < 10 && field.val().length === 1 ) {

                    field.val("0" + field.val() );
                }
            });

            /* Filtro numérico para los campos del login */
            $('#login-form .login-form__field input' ).numeric({
                allowDecSep: false,
                allowMinus: false
            });


            /* Validación del formulario Login */
            var login_validate = $('#login-form').validate(validateFormLogin);


            /* Validación para la fecha de nacimiento */
            $('#dni').on('focus', function() {

                $('#birth_date').val(
                    [
                        $('#year_born').val(),
                        $('#month_born').val(),
                        $('#day_born').val()
                    ].join('-')
                );
            });

            login_validate.element('input[name="birth_date"]');


            /* Eventos Submit del Login */
            $('#btn_login').on('click', function(e) {

                e.preventDefault();

                var self = $(this);
                    self.prop('disabled', 'disabled');

                $('#birth_date').val(
                    [
                        $('#year_born').val(),
                        $('#month_born').val(),
                        $('#day_born').val()
                    ].join('-')
                );

                if ( $('#login__form').valid() )  {

                    self.removeProp('disabled');
                    $('#login__form').submit();

                } else {

                    self.removeProp('disabled');
                }
            });

        }
    },


    /* Register */
    register: {
        init: function() {

            /* Filtrado de caracteres numéricos */
            $('#mobile, #dni, #day_born, #month_born, #year_born').numeric();

            /* Filtrado de caracteres alpha */
            $('#name, #lastname_mother, #lastname_father').alpha({
                allowSpace: " "
            });

            /* Validacion del formulario Registro */
            $('#register-form').validate( validateFormRegister );


            /* Configuración para enviar el Token CSRF por AJAX */
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            };

            var csrftoken = getCookie('csrftoken');

            function csrfSafeMethod(method) {
                // these HTTP methods do not require CSRF protection
                return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
            };


            /* Eventos Submit del Register */
            $('#btn_register').on('click', function(e) {
                e.preventDefault();
                var self = $(this);

                self.prop('disabled', 'disabled');

                if ( $('#register-form').valid() )  {

                    self.removeProp('disabled');

                    /* AJAX POST */
                     $.ajax({
                        type : 'POST',
                        url  : '/register/process/', // the url where we want to POST
                        data : JSON.stringify(data.form), // our data object
                        dataType    : 'json',
                        contentType : "application/json; charset=utf-8",

                        beforeSend: function(xhr, settings) {
                            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                                xhr.setRequestHeader("X-CSRFToken", csrftoken);
                            }
                        },

                        error:function (a1, a2, a3){
                            console.log('Server request error : '+a1+' | '+a2);
                        },

                        success:  function(data) {
                        }
                    });

                } else {

                    self.removeProp('disabled');
                }
            });
        }
    },

    /**
     * @param  {String} anchor  Id de la capa a scrollear
     * @return {...}
     */
    scrollToAnchor: function ( anchor ) {
        var ancla = $(anchor);

        $("html, body").animate({
            scrollTop: ancla.offset().top - 75
        }, 700);
    }
};


/* Ejecutador de metodos según vista */
UTIL = {
    exec: function(controller, action) {
        var ns = APP;

        action = (action === undefined ? "init" : action);

        if (controller !== "" && ns[controller] && typeof ns[controller][action] === "function") {
            ns[controller][action]();
        }
    },

    init: function() {
        var body       = document.getElementById("identify"),
            controller = body.getAttribute("data-controller"),
            action     = body.getAttribute("data-action");

        UTIL.exec("common");
        UTIL.exec(controller);
        UTIL.exec(controller, action);
    }
};

$(document).on("ready", UTIL.init);
